//
//  StretchyHeaderController.swift
//  BlurryHeader
//
//  Created by Taylor Miller-Klugman on 12/28/18.
//  Copyright © 2018 Taylor Miller-Klugman. All rights reserved.
//

import UIKit

fileprivate let cellID = "CellID"
fileprivate let HeaderID = "HeaderID"
fileprivate let padding: CGFloat = 16


class StretchyHeaderController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionViewSetup()
        setupLayout()
        
    }
    
    
    fileprivate func collectionViewSetup() {
        collectionView.backgroundColor = .white
        
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: cellID)
        collectionView.register(CollectionViewHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: HeaderID)
        collectionView.contentInsetAdjustmentBehavior = .never
    }
    
    fileprivate func setupLayout() {
        if let layout =  collectionViewLayout as? UICollectionViewFlowLayout {
            layout.sectionInset = .init(top: padding, left: padding, bottom: padding, right: padding)
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 18
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath)
        cell.backgroundColor = .black
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: view.frame.width - 2 * padding , height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return .init(width: view.frame.width, height: 340)
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: HeaderID, for: indexPath)
        return header
    }
}
