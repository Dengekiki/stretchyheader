//
//  CollectionViewHeader.swift
//  BlurryHeader
//
//  Created by Taylor Miller-Klugman on 12/28/18.
//  Copyright © 2018 Taylor Miller-Klugman. All rights reserved.
//

import UIKit

class CollectionViewHeader: UICollectionReusableView {
    
    let imageView: UIImageView = {
        let iv = UIImageView(image: #imageLiteral(resourceName: "headerimg"))
        iv.contentMode = .scaleAspectFill
        return iv
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .red
        addSubview(imageView)
        
        imageView.fillSuperview()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
